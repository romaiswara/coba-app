import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final PackageInfo packageInfo = await PackageInfo.fromPlatform();

  PackageInformation packageInformation = PackageInformation(
    appName: packageInfo.appName,
    packageName: packageInfo.packageName,
    appVersion: packageInfo.version,
    buildNumber: packageInfo.buildNumber,
  );

  runApp(MyApp(packageInformation: packageInformation));
}

class MyApp extends StatelessWidget {
  final PackageInformation packageInformation;

  MyApp({
    this.packageInformation,
  });

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Coba App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Coba App', package: packageInformation),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.package}) : super(key: key);
  final String title;
  final PackageInformation package;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
            SizedBox(height: 36),
            Text(
              'package info:\n${widget.package.toString()}',
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class PackageInformation {
  final String appVersion;
  final String buildNumber;
  final String packageName;
  final String appName;

  PackageInformation({
    this.appVersion,
    this.buildNumber,
    this.packageName,
    this.appName,
  });

  @override
  String toString() {
    return 'appVersion: $appVersion, \nbuildNumber: $buildNumber, \npackageName: $packageName, \nappName: $appName';
  }
}
